#!/bin/sh
IMG_DIR=/home/tru/singularity-img
DEF_DIR=/home/tru/singularity.d
O=`basename $0 .sh`
IMG_FILE=${IMG_DIR}/${O}.img
IMG_DEF=${DEF_DIR}/${O}.def
IMG_LOG=${DEF_DIR}/${O}.log
echo ${IMG_FILE}
\rm -f ${IMG_FILE}
sudo singularity create --size 12000 ${IMG_FILE}
sudo singularity bootstrap ${IMG_FILE} ${IMG_DEF} 2>&1 | tee ${IMG_LOG}
cat<<EOF | singularity exec ${IMG_FILE} python
# Creates a graph.
import tensorflow as tf
a = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[2, 3], name='a')
b = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[3, 2], name='b')
c = tf.matmul(a, b)
# Creates a session with log_device_placement set to True.
sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
# Runs the op.
print sess.run(c)
EOF
